import mysql.connector

db = mysql.connector.connect(
    host="localhost",
    user="lybekk",
    password="11234_4321password__PWD_PASS_WORD_p455w0rd",
    database="cinematic"
)

with db:
    cursor = db.cursor()
    select_movies = """
        SELECT * 
        FROM movies
        WHERE year >= 2002
        ORDER BY year;
    """
    cursor.execute(select_movies)
    data = cursor.fetchall()
    for row in data:
        print(row)
