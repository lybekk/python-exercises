import mysql.connector

db = mysql.connector.connect(
    host="localhost",
    user="lybekk",
    password="11234_4321password__PWD_PASS_WORD_p455w0rd",
    database="cinematic"
)

drop_movies = """DROP TABLE movies;"""
drop_directors = """DROP TABLE directors;"""

with db:
    cursor = db.cursor()
    cursor.execute(drop_movies)
    cursor.execute(drop_directors)
