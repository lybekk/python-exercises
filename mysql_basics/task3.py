sql_query = """
CREATE TABLE IF NOT EXISTS directors(
    director_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    name VARCHAR(30) NOT NULL,
    surname VARCHAR(30) NOT NULL,
    rating INT NOT NULL
);
CREATE TABLE IF NOT EXISTS movies(
    movie_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    title VARCHAR(30) NOT NULL,
    year INT UNSIGNED NOT NULL,
    category VARCHAR(30) NOT NULL,
    director_id INT NOT NULL,
    rating INT NOT NULL,
    FOREIGN KEY (director_id) REFERENCES directors(director_id)
);
"""
