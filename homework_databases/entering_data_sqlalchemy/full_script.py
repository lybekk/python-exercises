"""
DROP TABLE car_rental.bookings, car_rental.cars, car_rental.clients;
"""
from sqlalchemy import create_engine, inspect, select
from sqlalchemy import Column, String, Text, Integer, Date, DateTime, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


"""Create engine and connection object"""
user = "lybekk"
pwd = "11234_4321password__PWD_PASS_WORD_p455w0rd"
host = "localhost"
database = "car_rental"
engine = create_engine(f'mysql+mysqlconnector://{user}:{pwd}@{host}/{database}')

"""Define tables"""
base = declarative_base()


class Cars(base):
    __tablename__ = "cars"

    car_id = Column(Integer, primary_key=True)
    producer = Column(String(30))
    model = Column(String(30))
    year = Column(Integer)
    horse_power = Column(Integer)
    price_per_day = Column(Integer)


class Clients(base):
    __tablename__ = "clients"

    client_id = Column(Integer, primary_key=True)
    name = Column(String(30))
    surname = Column(String(30))
    address = Column(Text)
    city = Column(String(30))


class Bookings(base):
    __tablename__ = "bookings"

    booking_id = Column(Integer, primary_key=True)
    client_id = Column(Integer)
    car_id = Column(Integer)
    start_date = Column(Date)
    end_date = Column(Date)
    total_amount = Column(Integer)


"""Drop any existing table to avoid conflicts"""
Bookings.__table__.drop(engine)
Cars.__table__.drop(engine)
Clients.__table__.drop(engine)


"""Create tables"""
base.metadata.create_all(engine)


"""Alter tables"""
sql_queries_alter_table = [
    "ALTER TABLE clients MODIFY COLUMN client_id INTEGER AUTO_INCREMENT;",
    "ALTER TABLE cars MODIFY COLUMN car_id INTEGER AUTO_INCREMENT;",
    "ALTER TABLE bookings MODIFY COLUMN booking_id INTEGER AUTO_INCREMENT;",
    """
    ALTER TABLE bookings 
    ADD CONSTRAINT client_id_fk FOREIGN KEY (client_id) REFERENCES clients (client_id),
    ADD CONSTRAINT car_id_fk FOREIGN KEY (car_id) REFERENCES cars (car_id);
    """,
]

with engine.connect() as connection:
    for query in sql_queries_alter_table:
        connection.execute(query)


"""Create a session to make multiple commands with the same connection"""
Session = sessionmaker(bind=engine)
session = Session()

session.add_all([
    Clients(name="John", surname="Smith", address="Wall Street 12", city="New York"),
    Clients(name="Andrew", surname="Scott", address="Queen Victoria Street 1", city="London"),
    Cars(producer="Seat", model="Leon", year=2016, horse_power=80, price_per_day=200),
    Cars(producer="Toyota", model="Avensis", year=2014, horse_power=72, price_per_day=100),
])

session.commit()

session.add_all([
    Bookings(client_id=1, car_id=2, start_date="2020-07-05", end_date="2020-07-06", total_amount=100),
    Bookings(client_id=2, car_id=2, start_date="2020-07-10", end_date="2020-07-12", total_amount=200),
])

session.commit()
session.close()

inspector = inspect(engine)
for table_name in inspector.get_table_names():
    print("Table: ", table_name)
    for column in inspector.get_columns(table_name):
        print("Column: %s" % column['name'])
