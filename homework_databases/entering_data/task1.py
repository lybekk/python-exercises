import mysql.connector

db = mysql.connector.connect(
    host="localhost",
    user="lybekk",
    password="11234_4321password__PWD_PASS_WORD_p455w0rd",
    database="car_rental",
)

sql_query_clients = """
    INSERT INTO clients (name, surname, address, city) VALUES
        ('John', 'Smith', 'Wall Street 12', 'New York'), 
        ('Andrew', 'Scott', 'Queen Victoria Street 1', 'London' );
"""
sql_query_cars = """
    INSERT INTO cars (producer, model, year, horse_power, price_per_day) VALUES
        ('Seat', 'Leon', 2016, 80, 200),
        ('Toyota', 'Avensis', 2014, 72, 100);
"""
sql_query_bookings = """
    INSERT INTO bookings (client_id, car_id, start_date, end_date, total_amount) VALUES
        (1, 2, '2020-07-05', '2020-07-06', 100),
        (2, 2, '2020-07-10', '2020-07-12', 200);
"""

with db:
    cursor = db.cursor()
    cursor.execute(sql_query_clients)
    db.commit()
    cursor.execute(sql_query_cars)
    db.commit()
    cursor.execute(sql_query_bookings)
    db.commit()
