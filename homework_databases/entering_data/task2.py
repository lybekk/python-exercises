import mysql.connector

db = mysql.connector.connect(
    host="localhost",
    user="lybekk",
    password="11234_4321password__PWD_PASS_WORD_p455w0rd",
    database="car_rental",
)

sql_queries = [
    """
    ALTER TABLE clients 
        MODIFY COLUMN name VARCHAR(30) NOT NULL,
        MODIFY COLUMN surname VARCHAR(30) NOT NULL,
        MODIFY COLUMN address TEXT NOT NULL,
        MODIFY COLUMN city VARCHAR(30) NOT NULL;
    """,
    """
    ALTER TABLE cars 
        MODIFY COLUMN producer VARCHAR(30) NOT NULL,
        MODIFY COLUMN model VARCHAR(30) NOT NULL,
        MODIFY COLUMN year INTEGER UNSIGNED NOT NULL,
        MODIFY COLUMN horse_power INTEGER UNSIGNED NOT NULL,
        MODIFY COLUMN price_per_day INTEGER UNSIGNED NOT NULL;
    """,
    """
    SET FOREIGN_KEY_CHECKS = 0;
    
    ALTER TABLE bookings
        MODIFY COLUMN car_id INTEGER NOT NULL,
        MODIFY COLUMN client_id INTEGER NOT NULL,
        MODIFY COLUMN start_date DATE NOT NULL,
        MODIFY COLUMN end_date DATE NOT NULL,
        MODIFY COLUMN total_amount INTEGER UNSIGNED NOT NULL;
    
     SET FOREIGN_KEY_CHECKS = 1;
    """
]

with db:
    cursor = db.cursor()
    for query in sql_queries:
        cursor.execute(query)
