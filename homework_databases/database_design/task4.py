import mysql.connector

db = mysql.connector.connect(
    host="localhost",
    user="lybekk",
    password="11234_4321password__PWD_PASS_WORD_p455w0rd",
    database="car_rental",
)

sql_query = """
ALTER TABLE cars MODIFY COLUMN car_id INTEGER AUTO_INCREMENT;
ALTER TABLE clients MODIFY COLUMN client_id INTEGER AUTO_INCREMENT;
ALTER TABLE bookings MODIFY COLUMN booking_id INTEGER AUTO_INCREMENT;
"""

with db:
    cursor = db.cursor()
    cursor.execute(sql_query)
