import mysql.connector

db = mysql.connector.connect(
    host="localhost",
    user="lybekk",
    password="11234_4321password__PWD_PASS_WORD_p455w0rd",
    database="car_rental"
)

sql_query = """
    CREATE TABLE IF NOT EXISTS cars (
        car_id INTEGER PRIMARY KEY,
        producer VARCHAR(30),
        model VARCHAR(30),
        year INTEGER,
        horse_power INTEGER,
        price_per_day INTEGER
    );
    CREATE TABLE IF NOT EXISTS clients (
        client_id INTEGER PRIMARY KEY,
        name VARCHAR(30),
        surname VARCHAR(30),
        address TEXT,
        city VARCHAR(30)
    );
    CREATE TABLE IF NOT EXISTS bookings (
        booking_id INTEGER PRIMARY KEY,
        client_id INTEGER,
        car_id INTEGER,
        start_date DATE,
        end_date DATE,
        total_amount INTEGER
    );
"""

with db:
    cursor = db.cursor()
    cursor.execute(sql_query, multi=True)

