import mysql.connector

db = mysql.connector.connect(
    host="localhost",
    user="lybekk",
    password="11234_4321password__PWD_PASS_WORD_p455w0rd",
    database="car_rental",
)

sql_query = """
ALTER TABLE bookings 
    ADD CONSTRAINT client_id_fk FOREIGN KEY (client_id) REFERENCES clients (client_id),
    ADD CONSTRAINT car_id_fk FOREIGN KEY (car_id) REFERENCES cars (car_id);
"""

with db:
    cursor = db.cursor()
    cursor.execute(sql_query)
